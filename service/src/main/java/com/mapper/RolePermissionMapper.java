package com.mapper;

import com.entity.PermissionEntity;
import com.entity.RolePermissionEntity;
import tk.mybatis.mapper.common.BaseMapper;

import java.util.List;

public interface RolePermissionMapper extends BaseMapper<List<RolePermissionEntity>> {
}
