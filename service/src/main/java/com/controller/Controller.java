package com.controller;

import com.alibaba.fastjson.JSONObject;
import com.entity.PermissionEntity;
import com.entity.RoleEntity;
import com.service.MService;
import com.service.RolePermissionService;
import com.service.UserRoleService;
import com.utils.Result;
import com.utils.StatusCode;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/admin/service/index")
public class Controller {

    @Resource
    MService mService;
    private UserRoleService userRoleService;
    private RolePermissionService rolePermissionService;

    /*
    * 获取用户信息
    */
    @GetMapping("/userInfo")
    public Map getUserInfo(){
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        Map<String, Object> userInfo = mService.getUserInfo(username);
        return userInfo;
    }
    /*
    * 新增用户权限
    * */
    @PostMapping("/addpermission")
    public void addUserPerm(@RequestBody List<PermissionEntity> permissionlist,
                            @RequestParam(value = "uname") String username){
        RoleEntity role = userRoleService.getRoleByUserName(username);
        rolePermissionService.addPermissionlistByRoleId(role.getRid(),permissionlist);
        // 返回新增权限后的 permission list
//        return permissionEntityList;
    }
}
