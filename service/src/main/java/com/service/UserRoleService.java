package com.service;

import com.entity.RoleEntity;
import org.springframework.stereotype.Service;

@Service
public interface UserRoleService {
    /*
    * 根据用户id查询角色
    */
    RoleEntity getRoleByUserName(String uname);


}
