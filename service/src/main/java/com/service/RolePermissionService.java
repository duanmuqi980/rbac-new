package com.service;

import com.entity.PermissionEntity;

import java.util.List;

public interface RolePermissionService {

    /*
    * 通过角色 id查询权限列表
    */
    List<PermissionEntity> selectPermissionlistByRoleId(Integer rid);

    /*
    * 通过角色 id新增权限
    */
    void addPermissionlistByRoleId(Integer rid ,List<PermissionEntity> permissionEntityList);
}
