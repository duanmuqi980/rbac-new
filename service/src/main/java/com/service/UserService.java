package com.service;

import com.entity.UserEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * (User)表服务接口
 */
@Service
public interface UserService {

    /*
    * 从数据库中取出用户信息
    */
    UserEntity selectByUserName(String uname);

    /*
    * 通过用户名获取用户id
    */
    Integer selectUserIdByUserName(String uname);
}
