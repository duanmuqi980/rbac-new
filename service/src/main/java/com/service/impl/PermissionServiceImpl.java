package com.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.entity.PermissionEntity;
import com.entity.UserEntity;
import com.mapper.PermissionMapper;
import com.mapper.UserMapper;
import com.service.PermissionService;
import com.sun.javafx.tk.PermissionHelper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class PermissionServiceImpl implements PermissionService {

    @Autowired
    private PermissionMapper permissionMapper;
    @Autowired
    private UserMapper userMapper;

    @Override
    public List<String> selectPermissionValueByUserId(Integer id) {
        List<String> permissionValueList = null;
        List<PermissionEntity> temp = null;
        if(this.isAdmin(id)){
            temp = permissionMapper.selectAll();
            for(PermissionEntity p:temp){
                String permission = p.toString();
                permissionValueList.add(permission);
            }
            return permissionValueList;
        }
        else {
            permissionValueList.add(permissionMapper.selectByPrimaryKey(id).toString());
        }
        return permissionValueList;
    }

    /*
    * 判断是否为管理员
    */
    private boolean isAdmin(Integer uid){
        UserEntity user = userMapper.selectByPrimaryKey(uid);
        if(null != user && "admin".equals(user.getName())){
            return true;
        }
        return false;
    }
}
