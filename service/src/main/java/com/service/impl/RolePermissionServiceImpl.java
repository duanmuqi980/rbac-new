package com.service.impl;

import com.entity.PermissionEntity;
import com.entity.RolePermissionEntity;
import com.entity.UserEntity;
import com.mapper.PermissionMapper;
import com.mapper.RolePermissionMapper;
import com.service.RolePermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

public class RolePermissionServiceImpl implements RolePermissionService {

    @Autowired
    private RolePermissionMapper rolePermissionMapper;
    @Autowired
    private PermissionMapper permissionMapper;

    /*
     * 通过角色 id查询权限列表
     */
    @Override
    public List<PermissionEntity> selectPermissionlistByRoleId(Integer rid) {
        List<RolePermissionEntity> rolePermissionEntityList = rolePermissionMapper.selectByPrimaryKey(rid);
        List<PermissionEntity> permissionEntityList = null;
        for(RolePermissionEntity rolePermissionEntity : rolePermissionEntityList){
            Integer pid = rolePermissionEntity.getMid();
            PermissionEntity permissionEntity = permissionMapper.selectByPrimaryKey(pid);
            permissionEntityList.add(permissionEntity);
        }
        return permissionEntityList;
    }

    /*
     * 通过角色 id新增权限
     */
    @Override
    public void addPermissionlistByRoleId(Integer rid ,List<PermissionEntity> permissionEntityList) {
        List<RolePermissionEntity> rolePermissionEntityList = null;
        for (PermissionEntity permission : permissionEntityList){
            Integer pid = permission.getPid();
            RolePermissionEntity rolePermissionEntity = new RolePermissionEntity();
            rolePermissionEntity.setRid(rid);
            rolePermissionEntity.setMid(pid);
            rolePermissionEntityList.add(rolePermissionEntity);
        }
        rolePermissionMapper.insert(rolePermissionEntityList);
    }
}
