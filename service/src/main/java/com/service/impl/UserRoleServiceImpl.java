package com.service.impl;

import com.entity.RoleEntity;
import com.entity.UserRoleEntity;
import com.mapper.RoleMapper;
import com.mapper.UserRoleMapper;
import com.service.UserRoleService;
import com.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;

public class UserRoleServiceImpl implements UserRoleService {
    @Autowired
    private UserRoleMapper userRoleMapper;
    @Autowired
    private RoleMapper roleMapper;
    @Autowired
    private UserService userService;

    /*
     * 根据用户id查询角色
     */
    @Override
    public RoleEntity getRoleByUserName(String uname) {
        Integer uid = userService.selectUserIdByUserName(uname);
        UserRoleEntity roleUser = userRoleMapper.selectByPrimaryKey(uid);
        Integer roleId = roleUser.getRid();
        RoleEntity role = roleMapper.selectByPrimaryKey(roleId);
        return role;
    }
}
