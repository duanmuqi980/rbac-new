package com.service;

import java.util.Map;

public interface MService {
    /**
     * 根据用户名获取用户登录信息
     * @param uname
     * @return
     */
    Map<String, Object> getUserInfo(String uname);
}
