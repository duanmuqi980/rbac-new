package com.controller;

import com.entity.PermissionEntity;
import com.service.ConsumerService;
import com.utils.Result;
import com.utils.StatusCode;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class ConsumerController {

    private ConsumerService consumerService;

    /*
     * 获取用户信息 (返回结果)
     */
    @GetMapping("/result")
    public Result getUserInfo(){
        return new Result<Map>(true, StatusCode.OK,"已获取用户信息",consumerService.getUserInfo());
    }

    /*
    * 增加用户权限
    */
    @PostMapping("/adduserperm0")
    public Result addUser(@RequestBody List<PermissionEntity> permissionlist0,
                          @RequestParam(value = "uname") String username0){
        consumerService.addUserPerm(permissionlist0,username0);
        return new Result(true,StatusCode.OK,"新增成功");
    }
}
