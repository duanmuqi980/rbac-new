package com.service;

import com.entity.PermissionEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

@FeignClient("service")
public interface ConsumerService {
    /**
     * 根据用户名获取用户登录信息
     */
    @GetMapping("/userinfo")
    Map<String, Object> getUserInfo();

    /*
    * 新增用户权限
    */
    @PostMapping("/addpermission")
    void addUserPerm(List<PermissionEntity> permissionlist, String username);

}
