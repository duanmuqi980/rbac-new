package com.utils;

import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class DefaultPasswordEncoder implements PasswordEncoder {

    BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();

    @Override
    public String encode(CharSequence rawPassword){
        return bCryptPasswordEncoder.encode(rawPassword);
    }

//    @Override
//    public String encode(CharSequence rawPassword) {
//        // 生成随机密码盐
//        String salt = BCrypt.gensalt();
//        return BCrypt.hashpw(rawPassword.toString(), salt);
//    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        if (encodedPassword == null || encodedPassword.length() == 0) {
            return false;
        }
        return bCryptPasswordEncoder.matches(rawPassword, encodedPassword);
//        return BCrypt.checkpw(rawPassword.toString(), encodedPassword);
    }
}