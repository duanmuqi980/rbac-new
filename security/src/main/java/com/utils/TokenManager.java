package com.utils;

import io.jsonwebtoken.CompressionCodec;
import io.jsonwebtoken.CompressionCodecs;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;

/*
* token管理
*/
public class TokenManager {

    private final long nowMillis = System.currentTimeMillis();

    /*
    * 对应 JwtConstants.SECRET 属性
    * 可以由系统生成
    * https://blog.csdn.net/why15732625998/article/details/78534711*/
//    Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

    /**
    * @ref https://www.jianshu.com/p/1ebfc1d78928?utm_campaign=maleskine&utm_content=note&utm_medium=seo_notes&utm_source=recommendation
    * @param uname 用户名
    * @return token
    */
    public String createToken(String uname){
        String token = Jwts.builder().setSubject(uname)
                .setExpiration(new Date(nowMillis + JwtConstants.TIME_OUT))
                //签名方法  两个参数分别是签名算法和自定义的签名Key（盐）
                .signWith(SignatureAlgorithm.HS512,JwtConstants.SECRET)
                //当载荷过长时可对其进行压缩
                .compressWith(CompressionCodecs.GZIP)
                //生成JWT
                .compact();
        return token;
    }


    public String getUserFromToken(String token){
        String uname = Jwts.parser().setSigningKey(JwtConstants.SECRET)
                .parseClaimsJws(token)
                .getBody().getSubject();
        return uname;
    }

    public void removeToken(String token) {
    }

}