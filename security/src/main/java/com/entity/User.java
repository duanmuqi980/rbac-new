package com.entity;

import lombok.Data;
import java.io.Serializable;
/*
* 第三步
* 创建 users表对应的实体类
*/
@Data
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    private String username;

    private String password;
}