package com.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entity.UserEntity;
import org.springframework.stereotype.Repository;

@Repository//不加也可以，加了不让spring在注入时候爆红，看着不舒服，原因是UsersMapper是一个接口，实现类被mybatisplus封装了，spring不能识别
public interface UsersMapper extends BaseMapper<UserEntity> {
}
