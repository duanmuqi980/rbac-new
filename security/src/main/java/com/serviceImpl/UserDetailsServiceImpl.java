package com.serviceImpl;

import com.entity.SecurityUser;
import com.entity.User;
import com.entity.UserEntity;
import com.mapper.UserMapper;
import com.mapper.UsersMapper;
import com.service.PermissionService;
import com.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
/*
* 第二步
* 编写实现类，返回User对象，User对象有用户名密码和操作权限
*/
@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UsersMapper usersMapper;
    @Autowired
    private UserService userService;
    @Autowired
    private PermissionService permissionService;

/*
*       QueryWrapper<Users> wrapper = new QueryWrapper<>();
        wrapper.ge("username", username);
        Users users = usersMapper.selectOne(wrapper);
 */


    /*
    * 根据账号获取用户信息 <账号，密码，权限 >
    */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity user = userService.selectByUserName(username);
        if(null == user){
            throw new UsernameNotFoundException("用户名不存在！");
        }
        User curUser = new User();
        BeanUtils.copyProperties(user,curUser);
        List<String> authorities = permissionService.selectPermissionValueByUserId(user.getUid());
        SecurityUser securityUser = new SecurityUser(curUser);
        securityUser.setPermissionList(authorities);
        return securityUser;
    }
}
