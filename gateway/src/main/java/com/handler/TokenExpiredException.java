package com.handler;

import org.springframework.http.HttpStatus;
import org.springframework.lang.Nullable;
import org.springframework.web.server.ResponseStatusException;

/**
 * 令牌已经过期
 */
public class TokenExpiredException extends ResponseStatusException {
    //可以传入NULL值  当进行不安全的编码操作时，注释会给出警告
    public TokenExpiredException(@Nullable String reason) {
        //固定状态码  UNAUTHORIZED(401, "Unauthorized")
        super(HttpStatus.UNAUTHORIZED, reason);
    }
}